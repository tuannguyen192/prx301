<%-- 
    Document   : ListAllProduct
    Created on : Nov 6, 2017, 1:36:18 AM
    Author     : TuanNH
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<%
    String sql = "SELECT * FROM product";
    ArrayList<Integer> id = new ArrayList<Integer>();
    ArrayList<String> name = new ArrayList<String>();
    ArrayList<String> price = new ArrayList<String>();
    ArrayList<String> qty = new ArrayList<String>();
    try{
        Statement st=connect.createStatement();
        ResultSet rs=st.executeQuery(sql); 
        while(rs.next()){
        id.add(rs.getInt("id"));
        name.add(rs.getString("name"));
        price.add(rs.getString("price"));
        qty.add(rs.getString("qty"));
        }
    }catch(Exception ex){
        System.out.println(ex);
    }
%>
<!DOCTYPE html>
<table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Price</th>
            <th scope="col">Qty</th>
          </tr>
        </thead>
        <tbody>
            <% for(int i=0;i < id.size(); i++){ %>
          <tr>
            <td><% out.print(id.get(i)); %></td>
            <td><% out.print(name.get(i)); %></td>
            <td><% out.print(price.get(i)); %></td>
            <td><% out.print(qty.get(i)); %></td>
            <td><a href="EditProduct.jsp?id=<% out.print(id.get(i)); %>">Edit</a></td>
          </tr>
          <% } %>
        <tbody></tbody>
    </table>
    </body>
</html>
