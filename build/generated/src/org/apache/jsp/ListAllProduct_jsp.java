package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.*;
import lib.DBConect;
import java.sql.*;

public final class ListAllProduct_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(1);
    _jspx_dependants.add("/header.jsp");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");

    DBConect co = new DBConect();
    Connection connect = co.DBConect();

      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <link rel=\"stylesheet\" href=\"");
request.getContextPath();
      out.write("style.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css\" integrity=\"sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb\" crossorigin=\"anonymous\">\n");
      out.write("        <script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js\" integrity=\"sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js\" integrity=\"sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <div class=\"navbar\">\n");
      out.write("        <ul class=\"nav text-center\">\n");
      out.write("          <li class=\"item\">\n");
      out.write("            <a href=\"#\">Home</a>\n");
      out.write("          </li>\n");
      out.write("\n");
      out.write("          <li class=\"item\">\n");
      out.write("            <a href=\"#\">Blog</a>\n");
      out.write("          </li>\n");
      out.write("\n");
      out.write("          <li class=\"item\">\n");
      out.write("            <a href=\"#\">About</a>\n");
      out.write("          </li>\n");
      out.write("\n");
      out.write("          <li class=\"item\">\n");
      out.write("            <a href=\"#\" class=\"contact\">Contact</a>\n");
      out.write("          </li>\n");
      out.write("        </ul>\n");
      out.write("        </div>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("    \n");
      out.write('\n');

    String sql = "SELECT * FROM product";
    ArrayList<Integer> id = new ArrayList<Integer>();
    ArrayList<String> name = new ArrayList<String>();
    ArrayList<String> price = new ArrayList<String>();
    ArrayList<String> qty = new ArrayList<String>();
    try{
        Statement st=connect.createStatement();
        ResultSet rs=st.executeQuery(sql); 
        while(rs.next()){
        id.add(rs.getInt("id"));
        name.add(rs.getString("name"));
        price.add(rs.getString("price"));
        qty.add(rs.getString("qty"));
        System.out.println("a");
        }
    }catch(Exception ex){
        System.out.println(ex);
    }

      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<table class=\"table table-striped\">\n");
      out.write("        <thead>\n");
      out.write("          <tr>\n");
      out.write("            <th scope=\"col\">ID</th>\n");
      out.write("            <th scope=\"col\">Name</th>\n");
      out.write("            <th scope=\"col\">Price</th>\n");
      out.write("            <th scope=\"col\">Qty</th>\n");
      out.write("          </tr>\n");
      out.write("        </thead>\n");
      out.write("        <tbody>\n");
      out.write("            ");
 for(int i=0;i < id.size(); i++){ 
      out.write("\n");
      out.write("          <tr>\n");
      out.write("            <td>");
 out.print(id.get(i)); 
      out.write("</td>\n");
      out.write("            <td>");
 out.print(name.get(i)); 
      out.write("</td>\n");
      out.write("            <td>");
 out.print(price.get(i)); 
      out.write("</td>\n");
      out.write("            <td>");
 out.print(qty.get(i)); 
      out.write("</td>\n");
      out.write("            <td><a href=\"EditProduct.jsp?id=");
 out.print(id.get(i)); 
      out.write("\">Edit</a></td>\n");
      out.write("          </tr>\n");
      out.write("          ");
 } 
      out.write("\n");
      out.write("        <tbody></tbody>\n");
      out.write("    </table>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
