<%-- 
    Document   : ListHistory
    Created on : Nov 6, 2017, 3:23:48 AM
    Author     : TuanNH
--%>

<%@page import="java.time.LocalDate"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.time.ZonedDateTime"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<%
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    LocalDate localDate = LocalDate.now();

    String sql = "SELECT pr.name, ha.qty, ha.date FROM history_all  ha, product pr WHERE ha.id_product = pr.id AND date='"+localDate+"' ";
    ArrayList<String> name = new ArrayList<String>();
    ArrayList<String> date = new ArrayList<String>();
    ArrayList<String> qty = new ArrayList<String>();
    try{
        Statement st=connect.createStatement();
        ResultSet rs=st.executeQuery(sql); 
        while(rs.next()){
        name.add(rs.getString("pr.name"));
        qty.add(rs.getString("ha.qty"));
        date.add(rs.getString("ha.date"));
        }
    }catch(Exception ex){
        System.out.println(ex);
    }
%>

   <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Qty</th>
            <th scope="col">Date</th>
          </tr>
        </thead>
        <tbody>
            <% for(int i=0;i < name.size(); i++){ %>
          <tr>
            <td><% out.print(name.get(i)); %></td>
            <td><% out.print(qty.get(i)); %></td>
            <td><% out.print(date.get(i)); %></td>
          </tr>
          <% } %>
        <tbody></tbody>
    </table>
    </body>
</html>
